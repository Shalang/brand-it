import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { QuotationRequestVM } from './quotation-request-vm';

@Component({
  selector: 'app-quotationrequest',
  templateUrl: './quotationrequest.component.html',
  styleUrls: ['./quotationrequest.component.css']
})

export class QuotationrequestComponent implements OnInit {

  QuotationRequests : QuotationRequestVM[] = [];
  quotationrequestdetail: string[];
  constructor (private httpService: HttpClient, private router: Router) { }
  
  ngOnInit() {
    this.httpService.get<QuotationRequestVM[]>('https://localhost:44369/api/QuotationRequests/getQuotationRequests').subscribe (data => {

      console.log(data)
      this.QuotationRequests = data;

      // var result = Object.keys(data).map((key) =>  data[key]);
      // console.log(result)
      // var unique = [];
      // var distinct = [];
      // for( let i = 0; i < result.length; i++ ){
      //   if( !unique[result[i].Request_ID]){
      //     distinct.push(result[i]);
      //     unique[result[i].Request_ID] = 1;
      //   }
      // }
      // this.quotationrequests = distinct
      });
     // this.quotationrequests = data as string [];
  }
    
  
  requestdetails(id) {
    
    localStorage["Request_ID"] = id;
    this.httpService.get('https://localhost:44369/api/QuotationRequests/getQuotationResults/'+id).subscribe (res => {
      this.quotationrequestdetail = res as string [];
      console.log(this.quotationrequestdetail = res as string [])
    });
  }

  generate() {
    this.router.navigate(["/quotations"]);
  }
}
