import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Supplier } from 'src/app/5.Supplier/supplier/supplier.model';
import { SupplierorderService } from 'src/app/shared/supplierorder.service';
import { SupplierorderlineComponent } from '../supplierorderline/supplierorderline.component';

@Component({
  selector: 'app-supplierorder',
  templateUrl: './supplierorder.component.html',
  styleUrls: ['./supplierorder.component.css']
})
export class SupplierorderComponent implements OnInit {

  supList: Supplier[];
  isValid: boolean = true;
  supplier: any;
  TodayDate: string;

  constructor(public service: SupplierorderService,
    private dialog:MatDialog, 
     private router:Router, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.resetForm();
    this.TodayDate = new Date().toDateString();
    //const id = +this.route.snapshot.paramMap.get('id');
    this.service.getSupplier().then(res => this.supList = res as Supplier[]);
    //  console.log(this.designType)


     //this.customerService.getCustomer().then(res => this.cusList = res as Customer[]);
  }

  resetForm(form?:NgForm){
    if(form=null)
    form.resetForm();
    this.service.formData = {
      Supplier_Order_ID : null,
      Supplier_ID : 0,
      Supplier_Order_Status_ID : 1,
      Quotation_Line_ID : 1,
      Date: new Date(),
      Amount: 100,
    
    };
    this.service.orderLine=[];
  }

  AddOrder(orderLineIndex, Supplier_Order_ID){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.width="50%";
    dialogConfig.data = {orderLineIndex, Supplier_Order_ID};
  this.dialog.open(SupplierorderlineComponent, dialogConfig);
  
  }

  onDeleteOrderLine(orderLineID: number, i: number){
    //if (designLineID !=null)
    //this.service.formData.DeletedOrderItemIDs += designLineID + ",";
    this.service.orderLine.splice(i,1);
  }

  validateForm(){
    this.isValid = true;
    if(this.service.formData.Supplier_ID==0)
    this.isValid = false;
    else if(this.service.orderLine.length==0)
    this.isValid = false;
    else if(this.service.formData.Amount==0)
    this.isValid = false;
    return this.isValid;
  }

  onSubmit(form:NgForm){
    if(this.validateForm())
    {
      this.service.saveOrder().subscribe(res => {
        this.resetForm();
        console.log(res);
        // this.toastr.success('Product design saved','Success!');
        // this.router.navigate(['designs']);
      })
    }

  }
}
