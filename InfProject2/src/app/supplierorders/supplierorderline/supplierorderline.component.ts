import { Component, Inject, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Product } from 'src/app/product.model';
import { SupplierorderService } from 'src/app/shared/supplierorder.service';
import { Supplierorderline } from 'src/app/shared/supplierorderline.model';

@Component({
  selector: 'app-supplierorderline',
  templateUrl: './supplierorderline.component.html',
  styleUrls: ['./supplierorderline.component.css']
})
export class SupplierorderlineComponent implements OnInit {
  formData:Supplierorderline;
   typeList: Product[];
  // jobList: Jobtype[];
  isValid:  boolean = true;

  constructor(@Inject(MAT_DIALOG_DATA) public data,
  public dialogRef: MatDialogRef<SupplierorderlineComponent>, 
  public orderService: SupplierorderService) { }

  ngOnInit(): void {
    this.orderService.getProdTypeList().then(res => this.typeList = res as Product[]);
    // this.posService.getJobTypeList().then(res => this.jobList = res as Jobtype[]);
    if (this.data.orderLineIndex == null)
    this.formData = {
      Supplier_Order_Line_ID: null,
      Supplier_Order_ID: this.data.Supplier_Order_ID,
      Product_Type_ID: 0,
      Quantity: 0,
      Price: 0,
      Total: 0, 
    }
    else
    this.formData = Object.assign({}, this.orderService.orderLine[this.data.orderLineIndex]);

  }

  onSubmit(form: NgForm) {
    if (this.validateForm(form.value)) {
      if (this.data.orderLineIndex == null)
        this.orderService.orderLine.push(form.value);
      else
      this.orderService.orderLine[this.data.orderLineIndex] = form.value;
      this.dialogRef.close();
      console.log(form);
    }
  }
    validateForm(formData: Supplierorderline) {
      this.isValid = true;
      if (formData.Product_Type_ID == 0)
        this.isValid = false;
      else if (formData.Quantity == 0)
        this.isValid = false;
      else if (formData.Price <= 0)
        this.isValid = false;      
      return this.isValid;
    }
  }
