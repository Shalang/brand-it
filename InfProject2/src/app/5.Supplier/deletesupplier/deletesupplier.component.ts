import { Component, OnInit } from '@angular/core';
import { Supplier } from '../supplier/supplier.model';

import { Location } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from 'src/app/4.Product/category/category.service';
import { ErrorHandleService } from 'src/app/shared/errorhandle.service';
import { SuccessComponent } from 'src/app/shared/success/success.component';

@Component({
  selector: 'app-deletesupplier',
  templateUrl: './deletesupplier.component.html',
  styleUrls: ['./deletesupplier.component.css']
})
export class DeletesupplierComponent implements OnInit {

  constructor(private location: Location, private eService: CategoryService, private dialog: MatDialog, private errorService: ErrorHandleService,
    private activeRoute: ActivatedRoute) { }

  private dialogConfig;
  public supplier: Supplier;

ngOnInit() {
  this.dialogConfig = {
    height: '200px',
    width: '400px',
    disableClose: true,
    data: {}
  }

  this.getSupplierById();
}

public onCancel = () => {
  this.location.back();
}

private getSupplierById = () => {
  let Supplier_ID: string = this.activeRoute.snapshot.params['id'];
    
  let supplierByIdUrl: string = `Suppliers/${Supplier_ID}`;
 
  this.eService.getData(supplierByIdUrl)
    .subscribe(res => {
      this.supplier = res as Supplier;
    },
    (error) => {
      this.errorService.dialogConfig = this.dialogConfig;
      this.errorService.handleError(error);
    })
}

public delete = () => {
  let deleteUrl: string = `Suppliers/${this.supplier.Supplier_ID}`;
  this.eService.delete(deleteUrl)
    .subscribe(res => {
      let dialogRef = this.dialog.open(SuccessComponent, this.dialogConfig);
      
      dialogRef.afterClosed()
        .subscribe(result => {
          this.location.back();
        });
    },
    (error) => {
      this.errorService.dialogConfig = this.dialogConfig;
      this.errorService.handleError(error);
    })
}

}
