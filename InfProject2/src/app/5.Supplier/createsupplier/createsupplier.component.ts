import { Component, OnInit } from '@angular/core';
import { Supplier } from '../supplier/supplier.model';

import { Location } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { CategoryService } from 'src/app/4.Product/category/category.service';
import { ErrorHandleService } from 'src/app/shared/errorhandle.service';
import { SuccessComponent } from 'src/app/shared/success/success.component';
import { Weblinks } from '../weblinks.model';
import { SupplierService } from '../supplier/supplier.service';

@Component({
  selector: 'app-createsupplier',
  templateUrl: './createsupplier.component.html',
  styleUrls: ['./createsupplier.component.css']
})
export class CreatesupplierComponent implements OnInit {

  public supplierForm: FormGroup;
  private dialogConfig;
  webLinks : Weblinks[];
  web : Weblinks;

  constructor(private location: Location, private eService: CategoryService, private service: SupplierService,
    private dialog: MatDialog, private errorService: ErrorHandleService) { }

  ngOnInit() {
    this.supplierForm = new FormGroup({
      Supplier_Name: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      Contact_Number: new FormControl('', [Validators.required, Validators.maxLength(10)]),
      Email: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      Address: new FormControl('', [Validators.required, Validators.maxLength(60)]),
      Web_Link_ID: new FormControl(),
    });

    this.dialogConfig = {
      height: '200px',
      width: '400px',
      disableClose: true,
      data: { }
    }
    this.service.getWebLinks().subscribe( (weblinks) => {

      this.webLinks = weblinks;

    })

  }
 

 


  public hasError = (controlName: string, errorName: string) =>{
    return this.supplierForm.controls[controlName].hasError(errorName);
  }
 
  public onCancel = () => {
    this.location.back();
  }
 
  public createSupplier = (formValue) => {
    if (this.supplierForm.valid) {
      this.executeCreation(formValue);
    }
  }
 
  private executeCreation = (formValue) => {
    let supplier: Supplier = {
      Supplier_ID: formValue.Supplier_ID,
      Supplier_Name: formValue.Supplier_Name,
      Contact_Number: formValue.Contact_Number,
      Email: formValue.Email,
      Address: formValue.Address,
      Web_Link_ID: formValue.Web_Link_ID,
    }
 
    let apiUrl = 'Suppliers';
    this.eService.create(apiUrl, supplier)
      .subscribe(res => {
        let dialogRef = this.dialog.open(SuccessComponent, this.dialogConfig);
 
        //we are subscribing on the [mat-dialog-close] attribute as soon as we click on the dialog button
        dialogRef.afterClosed()
          .subscribe(result => {
            this.location.back();
          });
      },
    )
  }
 
}