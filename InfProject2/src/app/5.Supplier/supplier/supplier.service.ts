import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Weblinks } from '../weblinks.model';

@Injectable({
  providedIn: 'root'
})
export class SupplierService {

  constructor(private http: HttpClient) { }

  getWebLinks(){
    return this.http.get<Weblinks[]>(environment.ApiURL +'/Web_Link');
   }
}
