import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { CategoryService } from 'src/app/4.Product/category/category.service';
import { ErrorHandleService } from 'src/app/shared/errorhandle.service';
import { Supplier } from './supplier.model';

@Component({
  selector: 'app-supplier',
  templateUrl: './supplier.component.html',
  styleUrls: ['./supplier.component.css']
})
export class SupplierComponent implements OnInit {

  
  public displayedColumns = ['Supplier Name', 'Contact Number', 'Email', 'Address', 'Web Link', 'update', 'delete'];
  public dataSource = new MatTableDataSource<Supplier>();
  @ViewChild(MatSort) sort: MatSort;
 

  constructor(public eService: CategoryService, private errorService: ErrorHandleService, private router: Router) { }

  ngOnInit() {
    this.getAllSuppliers();
  }
 
  public getAllSuppliers = () => {
    this.eService.getData('Suppliers')
    .subscribe(res => {
      this.dataSource.data = res as Supplier[];
    },
    (error) => {
      this.errorService.handleError(error);
    })
    
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  public customSort = (event) => {
    console.log(event);
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }
 
  public details = (id: string) => {
    let url: string = `details/${id}`;
    this.router.navigate([url]);
  }
 
  public updateSupplier = (id: string) => {
    let updateUrl: string = `updatesupplier/${id}`;
    this.router.navigate([updateUrl]);
  }

  public deleteSupplier = (id: string) => {
    let url: string = `deletesupplier/${id}`;
    this.router.navigate([url]);
  }
 
}
