import { Component, OnInit } from '@angular/core';

import { Location } from '@angular/common';
import { Supplier } from '../supplier/supplier.model';
import { SuccessComponent } from 'src/app/shared/success/success.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from 'src/app/4.Product/category/category.service';
import { ErrorHandleService } from 'src/app/shared/errorhandle.service';

@Component({
  selector: 'app-updatesupplier',
  templateUrl: './updatesupplier.component.html',
  styleUrls: ['./updatesupplier.component.css']
})
export class UpdatesupplierComponent implements OnInit {

  public supplierForm: FormGroup;
  private dialogConfig;
  public supplier: Supplier;
  

  constructor(private location: Location, private eService: CategoryService, private dialog: MatDialog, private errorService: ErrorHandleService,
              private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.supplierForm = new FormGroup({
      Supplier_Name: new FormControl('', [Validators.required, Validators.maxLength(30)]),
      Contact_Number: new FormControl('', [Validators.required, Validators.maxLength(10)]),
      Email: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      Address: new FormControl('', [Validators.required, Validators.maxLength(60)]),
      Web_Link_ID: new FormControl('', [Validators.required, Validators.maxLength(2)]),
    });

    this.dialogConfig = {
      height: '200px',
      width: '400px',
      disableClose: true,
      data: {}
    }

    this.getSupplierById();
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.supplierForm.controls[controlName].hasError(errorName);
  }

  public onCancel = () => {
    this.location.back();
  }

  private getSupplierById = () => {
    let Supplier_ID: string = this.activeRoute.snapshot.params['id'];
      
    let supplierByIdUrl: string = `Suppliers/${Supplier_ID}`;
   
    this.eService.getData(supplierByIdUrl)
      .subscribe(res => {
        this.supplier = res as Supplier;
        this.supplierForm.patchValue(this.supplier);
      },
      (error) => {
        this.errorService.dialogConfig = this.dialogConfig;
        this.errorService.handleError(error);
      })
  }

  public updateSupplier = (formValue) => {
    if (this.supplierForm.valid) {
      this.executeUpdate(formValue);
    }
  }

  private executeUpdate = (formValue) => {

 
    this.supplier.Supplier_Name = formValue.Supplier_Name;
    this.supplier.Contact_Number = formValue.Contact_Number;
    this.supplier.Email = formValue.Email;
    this.supplier.Address = formValue.Address;
    this.supplier.Web_Link_ID = formValue.Web_Link_ID;
   
    let apiUrl = `Suppliers/${this.supplier.Supplier_ID}`;
    this.eService.update(apiUrl, this.supplier)
      .subscribe(res => {
        let dialogRef = this.dialog.open(SuccessComponent, this.dialogConfig);
        
        dialogRef.afterClosed()
          .subscribe(result => {
            this.location.back();
          });
      },
      (error => {
        this.errorService.dialogConfig = this.dialogConfig;
        this.errorService.handleError(error);
      })
    )
  }

}
