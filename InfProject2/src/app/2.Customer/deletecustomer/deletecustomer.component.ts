import { Component, OnInit } from '@angular/core';
import { Customer } from '../customer/customer.model';
import { CustomerService } from '../customer/customer.service';
import { MatDialog } from '@angular/material/dialog';
import { ErrorHandleService } from 'src/app/shared/errorhandle.service';
import { ActivatedRoute } from '@angular/router';
import { SuccessComponent } from 'src/app/shared/success/success.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-deletecustomer',
  templateUrl: './deletecustomer.component.html',
  styleUrls: ['./deletecustomer.component.css']
})
export class DeletecustomerComponent implements OnInit {

  constructor(private location: Location, private eService: CustomerService, private dialog: MatDialog, private errorService: ErrorHandleService,
    private activeRoute: ActivatedRoute) { }

  private dialogConfig;
  public customer: Customer;

ngOnInit() {
  this.dialogConfig = {
    height: '200px',
    width: '400px',
    disableClose: true,
    data: {}
  }

  this.getCustomerById();
}

public onCancel = () => {
  this.location.back();
}

private getCustomerById = () => {
  let Customer_ID: string = this.activeRoute.snapshot.params['id'];
    
  let customerByIdUrl: string = `Customers/${Customer_ID}`;
 
  this.eService.getData(customerByIdUrl)
    .subscribe(res => {
      this.customer = res as Customer;
    },
    (error) => {
      this.errorService.dialogConfig = this.dialogConfig;
      this.errorService.handleError(error);
    })
}

public deleteCustomer = () => {
  let deleteUrl: string = `Customers/${this.customer.Customer_ID}`;
  this.eService.delete(deleteUrl)
    .subscribe(res => {
      let dialogRef = this.dialog.open(SuccessComponent, this.dialogConfig);
      
      dialogRef.afterClosed()
        .subscribe(result => {
          this.location.back();
        });
    },
    (error) => {
      this.errorService.dialogConfig = this.dialogConfig;
      this.errorService.handleError(error);
    })
}

}