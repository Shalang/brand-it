export class Customer {

    Customer_ID: string;
    First_Name: string;
    Last_Name: string;
    Contact_Number: string;
    Email_Address: string;
    User_ID: number;
}
