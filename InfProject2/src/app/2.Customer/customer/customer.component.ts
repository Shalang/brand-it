import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Customer } from './customer.model';
import { CustomerService } from './customer.service';
import { ErrorHandleService } from 'src/app/shared/errorhandle.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  public displayedColumns = ['First Name','Last Name', 'Contact Number', 'Email', 'delete'];
  public dataSource = new MatTableDataSource<Customer>();
  @ViewChild(MatSort) sort: MatSort;

  constructor(public eService: CustomerService, private errorService: ErrorHandleService, private router: Router) { }

  ngOnInit() {
    this.getAllCustomers();
  }
 
  public getAllCustomers = () => {
    this.eService.getData('Customer')
    .subscribe(res => {
      this.dataSource.data = res as Customer[];
    },
    (error) => {
      this.errorService.handleError(error);
    })
    
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  public customSort = (event) => {
    console.log(event);
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }
 
  public details = (id: string) => {
    let url: string = `details/${id}`;
    this.router.navigate([url]);
  }
 
  public update = (id: string) => {
    let updateUrl: string = `update/${id}`;
    this.router.navigate([updateUrl]);
  }

  public deleteCustomer = (id: string) => {
    let url: string = `deletecustomer/${id}`;
    this.router.navigate([url]);
  }
 
}
