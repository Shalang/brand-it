import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { DesignVM } from './design';

@Injectable({
  providedIn: 'root'
})
export class DesignService {

  constructor(private http: HttpClient) { }

  GetDesign(id : Number){
    return this.http.get<DesignVM>( environment.ApiURL + "/Design/GetDesignByID/"+id);
  }

}
