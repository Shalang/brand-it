export class Supplierorder {
  Supplier_Order_ID: number;
  Supplier_ID: number;
  Supplier_Order_Status_ID: number;
  Quotation_Line_ID: number;
  Date: Date;
  Amount: number;
}
