export class Supplierorderline {
  Supplier_Order_Line_ID: number;
  Supplier_Order_ID: number;
  Product_Type_ID: number;
  Quantity: number;
  Price: number;
  Total: number;
}
