import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Supplierorder } from './supplierorder.model';
import { Supplierorderline } from './supplierorderline.model';

@Injectable({
  providedIn: 'root'
})
export class SupplierorderService {
  formData: Supplierorder;
  orderLine: Supplierorderline[];

  constructor(private http: HttpClient) { }

  saveOrder() {
    var body = {
      ...this.formData,
      Supplier_Order_Line: this.orderLine
    };
    return this.http.post(environment.ApiURL + '/Supplier_Order', body);
  }

  getSupplierList() {
    return this.http.get(environment.ApiURL + '/Supplier_Order').toPromise();
  }

  public getProdTypeList(){
    return this.http.get(environment.ApiURL + '/Product_Type').toPromise();
  }

  getSupplier() {
    return this.http.get(environment.ApiURL + '/Suppliers').toPromise();
  }

}
