import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SuccessComponent } from './success/success.component';
import { ErrorComponent } from './error/error.component';
import { ErrorhandleComponent } from './errorhandle/errorhandle.component';
//import { SuccessComponent } from '../success/success.component';
//import { ErrorComponent } from '../error/error.component';

 
@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
  ],
  exports: [
    FlexLayoutModule,
    SuccessComponent,
    ErrorComponent
  ],
  declarations: [SuccessComponent, ErrorComponent, ErrorhandleComponent],
  entryComponents: [
    SuccessComponent,
    ErrorComponent
  ]
})
export class SharedModule { }
