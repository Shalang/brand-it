import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Quotereqline } from './quotereqline.model';
import { map } from 'rxjs/operators';
import { Quotations } from './quotations.model';
import { QuoteVM } from './quote-vm';
import { Quotation } from './quotation';
import { QuoteLine } from './quote-line';

interface MyType {
  ID: number,
  Quotelines: QuoteLine[],
}

@Injectable({
  providedIn: 'root'
})
export class QuotationrequestService {
  reqLine: Quotereqline[];

  constructor(private http:HttpClient) { }

  getReqList() {
    return this.http.get(environment.ApiURL + '/Quotation_Request').toPromise();
  }

  // GetRequestID(id) {
  //   return this.http.get('https://localhost:44369/api/Quotation_Request/GetRequestID/'+id).pipe(map(result=>result));
  // }

  GetRequestID(id) {
    return this.http.get<QuoteVM>(environment.ApiURL + '/Quotation_Request/GetRequestID/'+id);
  }


  
  AddLine(obj: Quotations)
  {   
      // return this.http.post(environment.ApiURL + '/Quotations', line);
      // const httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})};
      // return this.http.post(environment.ApiURL + '/Quotations', element, httpOptions);
     
        return this.http.post(environment.ApiURL + '/Quotations', obj);
  }

  CreateNewQuote(newQuote : Quotation){

    return this.http.post<Quotation>(environment.ApiURL + '/Quotations' , newQuote).toPromise();

  }

  AddQuoteLines(quotelines : QuoteLine[] , id){

    var obj : MyType;
    obj = { ID : id , Quotelines : quotelines }

    return this.http.post(environment.ApiURL + '/Quotations/PostQuotationLines/' + id , quotelines)

  }
    
  
}