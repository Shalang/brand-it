export class Quotations {
    Quotation_Line_ID: number;
    Quotation_ID : number;
    Request_Line_ID: number;
    Description : string;
    Quantity : number;
    Price: number;
    Total: number;

}
