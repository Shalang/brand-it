import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { element } from 'protractor';
import { DesignVM } from '../shared/design';
import { DesignService } from '../shared/design.service';
import { Quotation } from './quotation';
import { QuotationrequestService } from './quotationrequest.service';
import { QuoteLine } from './quote-line';
import { QuoteLineVM } from './quote-line-vm';
import { QuoteVM } from './quote-vm';
import { Quotereqline } from './quotereqline.model';

@Component({
  selector: 'app-quotations',
  templateUrl: './quotations.component.html',
  styleUrls: ['./quotations.component.css']
})
export class QuotationsComponent implements OnInit {

  reqList : Object=null;
  number: Number
  Total=0
  Amount=0
  Prices = []
  Products = []
  linePrice=[]


  FinalTotal : number;
  LineTotal : number;
  RequestList : QuoteVM = new QuoteVM() ;
  Tester : QuoteVM = new QuoteVM() ;
  CurrentDesin : DesignVM;

  CurrentPrice

  constructor(public service:QuotationrequestService , public designService : DesignService , private sanitizer: DomSanitizer,) { }

  ngOnInit(){
    // this.service.GetRequestID(localStorage["Request_ID"]).subscribe(x=>{
    //   this.reqList = x;
    //   var result = Object.keys(x).map((key) =>  x[key]); 
    //   this.Products = result
    //   console.log(result)
    //   result.forEach(element=>{
    //     this.Prices.push(0)
    //   })
    // });
    this.service.GetRequestID(localStorage["Request_ID"]).subscribe( (quoteVM) => {
      this.RequestList = quoteVM;
      console.log(this.RequestList);
      
    })
  }
  onNumberChange(Price,Quantity,id){

    var isNumberLower : boolean = false;
    if (Price.target.value < 0) {
      Price.target.value = 0;
    }

    this.RequestList.QuoteTotal = 0;
    // this.Prices[i]=Quantity*Price.target.value;
    // this.linePrice[i]= Price.target.value
    // this.Total=this.Total+this.Prices[i]

    var newPrice = Quantity*Price.target.value;

    // let newObj = JSON.parse(
    //   JSON.stringify(this.RequestList.quoteLineVMs.find((qlvm) => a.shoppingListProductId == id))
    // );

    var currentLine = this.RequestList.quoteLineVMs.find( qlvm => qlvm.Request_Line_ID == id );

    if (currentLine.Price < parseInt(Price.target.value)) {
      isNumberLower = true;
    }

    currentLine.Total = newPrice;
    currentLine.Price = parseInt(Price.target.value)


    //this.RequestList.quoteLineVMs.find( qlvm => qlvm.Request_Line_ID = id ).Total = this.RequestList.quoteLineVMs.find( qlvm => qlvm.Request_Line_ID = id ).Total + newPrice;
    //this.RequestList.quoteLineVMs.find( qlvm => qlvm.Request_Line_ID = id ).Price = Price.target.value;


    this.RequestList.quoteLineVMs.forEach( (element) => {
      this.RequestList.QuoteTotal = this.RequestList.QuoteTotal + element.Total;

    });
  }

  ViewDesign(id){

    this.designService.GetDesign( id ).subscribe( (designVM) => {

      this.CurrentDesin = designVM;

    })

  }

  transform(image: string) {
    if (image) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(
        "data:image;base64," + image
      );
    } else {
      return "../../../../assets/NoImage.jpg";
    }
  }

  Submit()
  {
    var NewQuote = new Quotation();
    NewQuote.Amount = this.RequestList.QuoteTotal;
    NewQuote.Customer_ID = this.RequestList.CustomerID;

    this.service.CreateNewQuote(NewQuote).then( (newQuote) => {

      var QuoteLineList : QuoteLine[] = [];


      this.RequestList.quoteLineVMs.forEach( (quoteLine) => {

        var CurrentQuoteLine = new QuoteLine();

        CurrentQuoteLine.Total = quoteLine.Total;
        //CurrentQuoteLine.Quotation_Line_ID = quoteLine.Request_Line_ID;
        CurrentQuoteLine.Quotation_ID = newQuote.Quotation_ID;
        CurrentQuoteLine.Quantity = quoteLine.Quantity;
        CurrentQuoteLine.Price = quoteLine.Price;
        CurrentQuoteLine.Description = quoteLine.Description;
        CurrentQuoteLine.Design_ID = quoteLine.Design_ID;
        CurrentQuoteLine.Product_Size_ID = quoteLine.Product_Size_ID;
        CurrentQuoteLine.Web_Link_ID = quoteLine.Web_Link_ID;
        CurrentQuoteLine.Product_Code = quoteLine.Product_Code;
        CurrentQuoteLine.Product_Type_ID = quoteLine.Product_Type_ID;
        CurrentQuoteLine.Colour = quoteLine.Colour;

        QuoteLineList.push(CurrentQuoteLine);
      })

      return QuoteLineList
    }).then( (QuoteLineList) => {

      this.service.AddQuoteLines(QuoteLineList , this.RequestList.quoteLineVMs[0].Request_Line_ID).subscribe( (res) => {

        //Do something once it has been created

      })
    
    })

    // let  i=0
    // this.Products.forEach(element => {
    //   element.Price = Number(this.linePrice[i])
    //   element.Total = this.Prices[i]
    //   this.service.AddLine(element)
    //   console.log(element)
    //   i++;
    // });
    // console.log(this.Products)
  }
}