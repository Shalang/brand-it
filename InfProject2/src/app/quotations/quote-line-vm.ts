export class QuoteLineVM {

    constructor(){}

    Request_Line_ID : number;
    Product_Type : string;
    Product_Type_ID : number;
    Description : string;
    Quantity : number;
    Price : number;
    Total : number;
    Design_ID : number;
    Product_Size_ID : number;
    ProductSize : string;
    Colour : string;
    Web_Link_ID : number;
    WebLink : string;
    Product_Code : string;



}
