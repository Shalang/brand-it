export class QuoteLine {

    constructor(){}

    Quotation_Line_ID : number;
    Quotation_ID : number;
    Description : string;
    Quantity : number;
    Price : number;
    Total : number;
    Design_ID : number;
    Product_Code : string;
    Web_Link_ID : number;
    Product_Size_ID : number;
    Product_Type_ID : number;
    Colour : string;


    // public string Product_Code { get; set; }
    // public Nullable<int> Web_Link_ID { get; set; }
    // public Nullable<int> Product_Size_ID { get; set; }
    // public Nullable<int> Product_Type_ID { get; set; }

}
