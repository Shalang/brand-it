import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReturncusorderComponent } from './returncusorder.component';

describe('ReturncusorderComponent', () => {
  let component: ReturncusorderComponent;
  let fixture: ComponentFixture<ReturncusorderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReturncusorderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReturncusorderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
