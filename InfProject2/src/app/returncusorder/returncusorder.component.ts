import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {HttpClientModule, HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-returncusorder',
  templateUrl: './returncusorder.component.html',
  styleUrls: ['./returncusorder.component.css']
})
export class ReturncusorderComponent implements OnInit {

  custreturnorders: string[];
  constructor(private httpService: HttpClient) { }

  ngOnInit() {
    this.httpService.get('https://localhost:44369/api/Report/getReturnCusOrder').subscribe (data =>{
      this.custreturnorders = data as string [];
    });
  }

}

