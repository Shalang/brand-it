import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { ErrorHandleService } from 'src/app/shared/errorhandle.service';
import { Jobtype} from './jobtype.model';
import { CategoryService } from '../category/category.service';

@Component({
  selector: 'app-jobtype',
  templateUrl: './jobtype.component.html',
  styleUrls: ['./jobtype.component.css']
})
export class JobtypeComponent implements OnInit {

  public displayedColumns = ['ID','Description', 'details', 'update', 'delete'];
  public dataSource = new MatTableDataSource<Jobtype>();
  @ViewChild(MatSort) sort: MatSort;

  constructor(public eService: CategoryService, private errorService: ErrorHandleService, private router: Router) { }

 
  ngOnInit() {
    this.getJobTypes();
  }
 
  public getJobTypes = () => {
    this.eService.getData('Job_Type')
    .subscribe(res => {
      this.dataSource.data = res as Jobtype[];
    },
    (error) => {
      this.errorService.handleError(error);
    })
    
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  public customSort = (event) => {
    console.log(event);
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }
 
  public details = (id: string) => {
    let url: string = `details/${id}`;
    this.router.navigate([url]);
  }
 
  public updateJobType = (id: string) => {
    let updateUrl: string = `updatejobtype/${id}`;
    this.router.navigate([updateUrl]);
  }

  public deleteJobType = (id: string) => {
    let url: string = `deletejobtype/${id}`;
    this.router.navigate([url]);
  }
 
}