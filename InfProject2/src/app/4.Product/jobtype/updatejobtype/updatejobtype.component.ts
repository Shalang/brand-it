import { Component, OnInit } from '@angular/core';
import { ErrorHandleService } from 'src/app/shared/errorhandle.service';
import { SuccessComponent } from 'src/app/shared/success/success.component';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Jobtype } from '../jobtype.model';
import { CategoryService } from '../../category/category.service';



@Component({
  selector: 'app-updatejobtype',
  templateUrl: './updatejobtype.component.html',
  styleUrls: ['./updatejobtype.component.css']
})
export class UpdatejobtypeComponent implements OnInit {
  public jobtypeForm: FormGroup;
  private dialogConfig;
  public jobtype: Jobtype;

  constructor(private location: Location, private eService: CategoryService, private dialog: MatDialog, private errorService: ErrorHandleService,
    private activeRoute: ActivatedRoute) { }

    ngOnInit() {
      this.jobtypeForm = new FormGroup({
        Description: new FormControl('', [Validators.required, Validators.maxLength(20)]),
      });
  
      this.dialogConfig = {
        height: '200px',
        width: '400px',
        disableClose: true,
        data: {}
      }
  
      this.getJobtypeById();
    }
  
    public hasError = (controlName: string, errorName: string) => {
      return this.jobtypeForm.controls[controlName].hasError(errorName);
    }
  
    public onCancel = () => {
      this.location.back();
    }
  
    private getJobtypeById = () => {
      let Job_Type_ID: string = this.activeRoute.snapshot.params['id'];
        
      let jobtypeByIdUrl: string = `Job_Type/${Job_Type_ID}`;
     
      this.eService.getData(jobtypeByIdUrl)
        .subscribe(res => {
          this.jobtype = res as Jobtype;
          this.jobtypeForm.patchValue(this.jobtype);
        },
        (error) => {
          this.errorService.dialogConfig = this.dialogConfig;
          this.errorService.handleError(error);
        })
    }
  
    public updateJobtype = (formValue) => {
      if (this.jobtypeForm.valid) {
        this.executeUpdate(formValue);
      }
    }
  
    private executeUpdate = (formValue) => {
  
   
      this.jobtype.Description = formValue.Description;
     
      let apiUrl = `Job_Type/${this.jobtype.Job_Type_ID}`;
      this.eService.update(apiUrl, this.jobtype)
        .subscribe(res => {
          let dialogRef = this.dialog.open(SuccessComponent, this.dialogConfig);
          
          dialogRef.afterClosed()
            .subscribe(result => {
              this.location.back();
            });
        },
        (error => {
          this.errorService.dialogConfig = this.dialogConfig;
          this.errorService.handleError(error);
        })
      )
    }
  
  }
  