import { Component, OnInit } from '@angular/core';
import { SuccessComponent } from 'src/app/shared/success/success.component';
import { ErrorHandleService } from 'src/app/shared/errorhandle.service';
import { Location } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from '../../category/category.service';
import { Jobtype } from '../jobtype.model';

@Component({
  selector: 'app-deletejobtype',
  templateUrl: './deletejobtype.component.html',
  styleUrls: ['./deletejobtype.component.css']
})
export class DeletejobtypeComponent implements OnInit {

  constructor(private location: Location, private eService: CategoryService, private dialog: MatDialog, private errorService: ErrorHandleService,
    private activeRoute: ActivatedRoute) { }

  private dialogConfig;
  public jobtype: Jobtype;

  ngOnInit() {
    this.dialogConfig = {
      height: '200px',
      width: '400px',
      disableClose: true,
      data: {}
    }
  
    this.getJobtypeById();
  }
  
  public onCancel = () => {
    this.location.back();
  }
  
  private getJobtypeById = () => {
    let Job_Type_ID: string = this.activeRoute.snapshot.params['id'];
      
    let jobtypeByIdUrl: string = `Job_Type/${Job_Type_ID}`;
   
    this.eService.getData(jobtypeByIdUrl)
      .subscribe(res => {
        this.jobtype = res as Jobtype;
      },
      (error) => {
        this.errorService.dialogConfig = this.dialogConfig;
        this.errorService.handleError(error);
      })
  }
  
  public delete = () => {
    let deleteUrl: string = `Job_Type/${this.jobtype.Job_Type_ID}`;
    this.eService.delete(deleteUrl)
      .subscribe(res => {
        let dialogRef = this.dialog.open(SuccessComponent, this.dialogConfig);
        
        dialogRef.afterClosed()
          .subscribe(result => {
            this.location.back();
          });
      },
      (error) => {
        this.errorService.dialogConfig = this.dialogConfig;
        this.errorService.handleError(error);
      })
  }
}  
