import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { SuccessComponent } from 'src/app/shared/success/success.component';
import { ErrorHandleService } from 'src/app/shared/errorhandle.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CategoryService } from '../../category/category.service';
import { Jobtype } from '../jobtype.model';


@Component({
  selector: 'app-createjobtype',
  templateUrl: './createjobtype.component.html',
  styleUrls: ['./createjobtype.component.css']
})
export class CreatejobtypeComponent implements OnInit {

  public jobtypeForm: FormGroup;
  private dialogConfig;
  constructor(private location: Location, private eService: CategoryService, private dialog: MatDialog, private errorService: ErrorHandleService) { }

  ngOnInit() {
    this.jobtypeForm = new FormGroup({
      Description: new FormControl('', [Validators.required, Validators.maxLength(20)]),
    });

    this.dialogConfig = {
      height: '200px',
      width: '400px',
      disableClose: true,
      data: { }
    }
  }
 
  public hasError = (controlName: string, errorName: string) =>{
    return this.jobtypeForm.controls[controlName].hasError(errorName);
  }
 
  public onCancel = () => {
    this.location.back();
  }
 
  public createJobtype = (formValue) => {
    if (this.jobtypeForm.valid) {
      this.executeCreation(formValue);
    }
  }
 
  private executeCreation = (formValue) => {
    let jobtype: Jobtype = {
      Job_Type_ID: formValue.Job_Type_ID,
      Description: formValue.Description
    }
 
    let apiUrl = 'Job_Type';
    this.eService.create(apiUrl, jobtype)
      .subscribe(res => {
        let dialogRef = this.dialog.open(SuccessComponent, this.dialogConfig);
 
        //we are subscribing on the [mat-dialog-close] attribute as soon as we click on the dialog button
        dialogRef.afterClosed()
          .subscribe(result => {
            this.location.back();
          });
      },
    )
  }
 
}
