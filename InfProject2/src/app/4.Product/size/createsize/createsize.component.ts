import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { SuccessComponent } from 'src/app/shared/success/success.component';
import { ErrorHandleService } from 'src/app/shared/errorhandle.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CategoryService } from '../../category/category.service';
import { Size } from '../size.model';


@Component({
  selector: 'app-createsize',
  templateUrl: './createsize.component.html',
  styleUrls: ['./createsize.component.css']
})
export class CreatesizeComponent implements OnInit {

  public sizeForm: FormGroup;
  private dialogConfig;

  constructor(private location: Location, private eService: CategoryService, private dialog: MatDialog, private errorService: ErrorHandleService) { }

  ngOnInit() {
    this.sizeForm = new FormGroup({
      Description: new FormControl('', [Validators.required, Validators.maxLength(20)]),
    });

    this.dialogConfig = {
      height: '200px',
      width: '400px',
      disableClose: true,
      data: { }
    }
  }
 
  public hasError = (controlName: string, errorName: string) =>{
    return this.sizeForm.controls[controlName].hasError(errorName);
  }
 
  public onCancel = () => {
    this.location.back();
  }
 
  public createSize = (formValue) => {
    if (this.sizeForm.valid) {
      this.executeCreation(formValue);
    }
  }
 
  private executeCreation = (formValue) => {
    let size: Size = {
      Product_Size_ID: formValue.Product_Size_ID,
      Description: formValue.Description
    }
 
    let apiUrl = 'Product_Size';
    this.eService.create(apiUrl, size)
      .subscribe(res => {
        let dialogRef = this.dialog.open(SuccessComponent, this.dialogConfig);
 
        //we are subscribing on the [mat-dialog-close] attribute as soon as we click on the dialog button
        dialogRef.afterClosed()
          .subscribe(result => {
            this.location.back();
          });
      },
    )
  }
 
}