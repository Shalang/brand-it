import { Component, OnInit } from '@angular/core';
import { ErrorHandleService } from 'src/app/shared/errorhandle.service';
import { SuccessComponent } from 'src/app/shared/success/success.component';

import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from '../../category/category.service';
import { Size } from '../size.model';

@Component({
  selector: 'app-updatesize',
  templateUrl: './updatesize.component.html',
  styleUrls: ['./updatesize.component.css']
})
export class UpdatesizeComponent implements OnInit {
  public sizeForm: FormGroup;
  private dialogConfig;
  public size: Size;
  

  constructor(private location: Location, private eService: CategoryService, private dialog: MatDialog, private errorService: ErrorHandleService,
    private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.sizeForm = new FormGroup({
      Description: new FormControl('', [Validators.required, Validators.maxLength(20)]),
    });

    this.dialogConfig = {
      height: '200px',
      width: '400px',
      disableClose: true,
      data: {}
    }

    this.getSizeById();
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.sizeForm.controls[controlName].hasError(errorName);
  }

  public onCancel = () => {
    this.location.back();
  }

  private getSizeById = () => {
    let Product_Size_ID: string = this.activeRoute.snapshot.params['id'];
      
    let sizeByIdUrl: string = `Product_Size/${Product_Size_ID}`;
   
    this.eService.getData(sizeByIdUrl)
      .subscribe(res => {
        this.size = res as Size;
        this.sizeForm.patchValue(this.size);
      },
      (error) => {
        this.errorService.dialogConfig = this.dialogConfig;
        this.errorService.handleError(error);
      })
  }

  public updateSize= (formValue) => {
    if (this.sizeForm.valid) {
      this.executeUpdate(formValue);
    }
  }

  private executeUpdate = (formValue) => {

 
    this.size.Description = formValue.Description;
   
    let apiUrl = `Product_Size/${this.size.Product_Size_ID}`;
    this.eService.update(apiUrl, this.size)
      .subscribe(res => {
        let dialogRef = this.dialog.open(SuccessComponent, this.dialogConfig);
        
        dialogRef.afterClosed()
          .subscribe(result => {
            this.location.back();
          });
      },
      (error => {
        this.errorService.dialogConfig = this.dialogConfig;
        this.errorService.handleError(error);
      })
    )
  }

}
