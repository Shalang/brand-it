import { Component, OnInit,ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { ErrorHandleService } from 'src/app/shared/errorhandle.service';
import { Size} from './size.model';
import { CategoryService } from '../category/category.service';

@Component({
  selector: 'app-size',
  templateUrl: './size.component.html',
  styleUrls: ['./size.component.css']
})
export class SizeComponent implements OnInit {

  public displayedColumns = ['ID','Description', 'details', 'update', 'delete'];
  public dataSource = new MatTableDataSource<Size>();
  @ViewChild(MatSort) sort: MatSort;

  constructor(public eService: CategoryService, private errorService: ErrorHandleService, private router: Router) { }

  ngOnInit() {
    this.getSizes();
  }
 
  public getSizes = () => {
    this.eService.getData('Product_Size')
    .subscribe(res => {
      this.dataSource.data = res as Size[];
    },
    (error) => {
      this.errorService.handleError(error);
    })
    
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  public customSort = (event) => {
    console.log(event);
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }
 
  public details = (id: string) => {
    let url: string = `details/${id}`;
    this.router.navigate([url]);
  }
 
  public updateSize = (id: string) => {
    let updateUrl: string = `updatesize/${id}`;
    this.router.navigate([updateUrl]);
  }

  public deleteSize = (id: string) => {
    let url: string = `deletesize/${id}`;
    this.router.navigate([url]);
  }
 
}