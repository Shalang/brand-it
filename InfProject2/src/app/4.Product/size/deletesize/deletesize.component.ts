import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Location } from '@angular/common';
import { ErrorHandleService } from 'src/app/shared/errorhandle.service';
import { SuccessComponent } from 'src/app/shared/success/success.component';
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from '../../category/category.service';
import { Size } from '../size.model';

@Component({
  selector: 'app-deletesize',
  templateUrl: './deletesize.component.html',
  styleUrls: ['./deletesize.component.css']
})
export class DeletesizeComponent implements OnInit {

  constructor(private location: Location, private eService: CategoryService, private dialog: MatDialog, private errorService: ErrorHandleService,
    private activeRoute: ActivatedRoute) { }

  private dialogConfig;
  public size: Size;

  ngOnInit() {
    this.dialogConfig = {
      height: '200px',
      width: '400px',
      disableClose: true,
      data: {}
    }
  
    this.getSizeById();
  }
  
  public onCancel = () => {
    this.location.back();
  }
  
  private getSizeById = () => {
    let Product_Size_ID: string = this.activeRoute.snapshot.params['id'];
      
    let sizeByIdUrl: string = `Product_Size/${Product_Size_ID}`;
   
    this.eService.getData(sizeByIdUrl)
      .subscribe(res => {
        this.size = res as Size;
      },
      (error) => {
        this.errorService.dialogConfig = this.dialogConfig;
        this.errorService.handleError(error);
      })
  }
  
  public delete = () => {
    let deleteUrl: string = `Product_Size/${this.size.Product_Size_ID}`;
    this.eService.delete(deleteUrl)
      .subscribe(res => {
        let dialogRef = this.dialog.open(SuccessComponent, this.dialogConfig);
        
        dialogRef.afterClosed()
          .subscribe(result => {
            this.location.back();
          });
      },
      (error) => {
        this.errorService.dialogConfig = this.dialogConfig;
        this.errorService.handleError(error);
      })
  }
  
  }
  