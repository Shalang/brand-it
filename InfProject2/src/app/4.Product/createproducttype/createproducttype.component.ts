import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Product } from 'src/app/product.model';
import { ErrorHandleService } from 'src/app/shared/errorhandle.service';
import { SuccessComponent } from 'src/app/shared/success/success.component';
import { CategoryService } from '../category/category.service';
import { Location } from '@angular/common';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-createproducttype',
  templateUrl: './createproducttype.component.html',
  styleUrls: ['./createproducttype.component.css']
})
export class CreateproducttypeComponent implements OnInit {

  public productForm: FormGroup;
  private dialogConfig;

  constructor(private location: Location, private eService: CategoryService,
     private dialog: MatDialog, private errorService: ErrorHandleService,
     private service: ProductsService) { }

  ngOnInit() {
    this.productForm = new FormGroup({
      Name: new FormControl('', [Validators.required, Validators.maxLength(20)]),
      Product_Category_ID: new FormControl(),
      Template_ID: new FormControl(),

    });

    this.dialogConfig = {
      height: '200px',
      width: '400px',
      disableClose: true,
      data: { }
    }
  }
 
  public hasError = (controlName: string, errorName: string) =>{
    return this.productForm.controls[controlName].hasError(errorName);
  }
 
  public onCancel = () => {
    this.location.back();
  }
 
  public createProduct = (formValue) => {
    if (this.productForm.valid) {
      this.executeCreation(formValue);
      this.upload();
    }
   
  }
 
  private executeCreation = (formValue) => {
    let product: Product = {
     
      Product_Type_ID: formValue.Product_Type_ID,
    Product_Category_ID : formValue.Product_Category_ID,
     Template_ID : formValue.Template_ID,
     Name : formValue.Name,
    ImageString : formValue.ImageString
      
    }
 
    let apiUrl = 'Product_Type';
    this.eService.create(apiUrl, product)
      .subscribe(res => {
        let dialogRef = this.dialog.open(SuccessComponent, this.dialogConfig);
 
        //we are subscribing on the [mat-dialog-close] attribute as soon as we click on the dialog button
        dialogRef.afterClosed()
          .subscribe(result => {
            this.location.back();
          });
      },
    )
  }





  upload(){
    this.service.upload(this.object.url).subscribe(x=>{
      console.log(x);
    })

  }

  public object: any = {};

  file(i){
    if(i.files && i.files[0]){
      var r = new FileReader();
      r.onload = (e : any) => {
        console.log(e.target.result)
        this.object.url = e.target.result;
      }
      r.readAsDataURL(i.files[0]);
    }

  }

}
 

