import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient) { }

  url = 'https://localhost:44369/api/Designs';

  upload(file){
    const httpOptions ={
      headers: new HttpHeaders({
        'Content-Type' : "application/json",
        'Access-Control-Origin': "*"
      })
    }
    return this.http.post<any>(`${this.url}/upload`, {file: file}, httpOptions);
  }
}
