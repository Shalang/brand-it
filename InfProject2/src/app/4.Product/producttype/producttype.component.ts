import { ReadVarExpr } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ProductsService }  from '../products.service';
@Component({
  selector: 'app-producttype',
  templateUrl: './producttype.component.html',
  styleUrls: ['./producttype.component.css']
})
export class ProducttypeComponent implements OnInit {

  constructor(private service: ProductsService) { }

  ngOnInit(){
  }

  upload(){
    this.service.upload(this.object.url).subscribe(x=>{
      console.log(x);
    })

  }

  public object: any = {};

  file(i){
    if(i.files && i.files[0]){
      var r = new FileReader();
      r.onload = (e : any) => {
        console.log(e.target.result)
        this.object.url = e.target.result;
      }
      r.readAsDataURL(i.files[0]);
    }

  }

}
