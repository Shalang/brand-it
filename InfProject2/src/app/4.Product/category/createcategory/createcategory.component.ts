import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators } from '@angular/forms';

import { Location } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { SuccessComponent } from 'src/app/shared/success/success.component';
import { ErrorHandleService } from 'src/app/shared/errorhandle.service';
import { CategoryService } from '../category.service';
import { Category } from '../category.model';
@Component({
  selector: 'app-createcategory',
  templateUrl: './createcategory.component.html',
  styleUrls: ['./createcategory.component.css']
})
export class CreatecategoryComponent implements OnInit {

  public categoryForm: FormGroup;
  private dialogConfig;

  constructor(private location: Location, private eService: CategoryService, private dialog: MatDialog, private errorService: ErrorHandleService) { }

  ngOnInit() {
    this.categoryForm = new FormGroup({
      Description: new FormControl('', [Validators.required, Validators.maxLength(20)]),
    });

    this.dialogConfig = {
      height: '200px',
      width: '400px',
      disableClose: true,
      data: { }
    }
  }
 
  public hasError = (controlName: string, errorName: string) =>{
    return this.categoryForm.controls[controlName].hasError(errorName);
  }
 
  public onCancel = () => {
    this.location.back();
  }
 
  public createCategory = (formValue) => {
    if (this.categoryForm.valid) {
      this.executeCreation(formValue);
    }
  }
 
  private executeCreation = (formValue) => {
    let category: Category = {
      Product_Category_ID: formValue.Product_Category_ID,
      Description: formValue.Description
    }
 
    let apiUrl = 'Product_Category';
    this.eService.create(apiUrl, category)
      .subscribe(res => {
        let dialogRef = this.dialog.open(SuccessComponent, this.dialogConfig);
 
        //we are subscribing on the [mat-dialog-close] attribute as soon as we click on the dialog button
        dialogRef.afterClosed()
          .subscribe(result => {
            this.location.back();
          });
      },
    )
  }
 
}