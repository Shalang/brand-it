import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//import { CategoryService } from '../4.Product/category/category.service';
import { MatDialog } from '@angular/material/dialog';
//import { ErrorHandleService } from '../shared/errorhandle.service';
//import { Category } from '../4.Product/category/category.model';
//import { SuccessComponent } from '../shared/success/success.component';
import { Location } from '@angular/common';

import { ErrorHandleService } from 'src/app/shared/errorhandle.service';

import { SuccessComponent } from 'src/app/shared/success/success.component';
import { CategoryService } from '../category.service';
import { Category } from '../category.model';

@Component({
  selector: 'app-deletecategory',
  templateUrl: './deletecategory.component.html',
  styleUrls: ['./deletecategory.component.css']
})
export class DeletecategoryComponent implements OnInit {

  constructor(private location: Location, private eService: CategoryService, private dialog: MatDialog, private errorService: ErrorHandleService,
    private activeRoute: ActivatedRoute) { }

  private dialogConfig;
  public category: Category;

ngOnInit() {
  this.dialogConfig = {
    height: '200px',
    width: '400px',
    disableClose: true,
    data: {}
  }

  this.getCategoryById();
}

public onCancel = () => {
  this.location.back();
}

private getCategoryById = () => {
  let Product_Category_ID: string = this.activeRoute.snapshot.params['id'];
    
  let categoryByIdUrl: string = `Product_Category/${Product_Category_ID}`;
 
  this.eService.getData(categoryByIdUrl)
    .subscribe(res => {
      this.category = res as Category;
    },
    (error) => {
      this.errorService.dialogConfig = this.dialogConfig;
      this.errorService.handleError(error);
    })
}

public delete = () => {
  let deleteUrl: string = `Product_Category/${this.category.Product_Category_ID}`;
  this.eService.delete(deleteUrl)
    .subscribe(res => {
      let dialogRef = this.dialog.open(SuccessComponent, this.dialogConfig);
      
      dialogRef.afterClosed()
        .subscribe(result => {
          this.location.back();
        });
    },
    (error) => {
      this.errorService.dialogConfig = this.dialogConfig;
      this.errorService.handleError(error);
    })
}

}
