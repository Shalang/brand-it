import { Component, OnInit, ViewChild } from '@angular/core';
import { CategoryService } from './category.service';
import { Category } from './category.model';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { ErrorHandleService } from 'src/app/shared/errorhandle.service';



@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  public displayedColumns = ['ID','Description', 'details', 'update', 'delete'];
  public dataSource = new MatTableDataSource<Category>();
  @ViewChild(MatSort) sort: MatSort;
 

  constructor(public eService: CategoryService, private errorService: ErrorHandleService, private router: Router) { }

  ngOnInit() {
    this.getAllCategories();
  }
 
  public getAllCategories = () => {
    this.eService.getData('Product_Category')
    .subscribe(res => {
      this.dataSource.data = res as Category[];
    },
    (error) => {
      this.errorService.handleError(error);
    })
    
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  public customSort = (event) => {
    console.log(event);
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }
 
  public details = (id: string) => {
    let url: string = `details/${id}`;
    this.router.navigate([url]);
  }
 
  public update = (id: string) => {
    let updateUrl: string = `update/${id}`;
    this.router.navigate([updateUrl]);
  }

  public delete = (id: string) => {
    let url: string = `delete/${id}`;
    this.router.navigate([url]);
  }
 
}


  /*ngOnInit() {
    this.resetForm();
    this.refreshList();
  }
  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  resetForm() {
    this.eService.categoryData = {
      Product_Category_ID: 0,
      Description: ''      
    }
    this.eService.categoryList = [];  
  }

  

  public refreshList = () => {
    this.eService.getData('Product_Category')
    .subscribe(res => {
      this.dataSource.data = res as Category[];
    })
  }

  editCategory(cat: Category) {
    this.eService.categoryData = cat;   
  }

  deleteCategory(id: number) {
    this.eService.delete(id).subscribe(res => {
      this.resetForm();
      this.refreshList();
    })
  }

  submit() {
    if(this.eService.categoryData.Product_Category_ID === 0) {
      this.eService.create(this.eService.categoryData).subscribe(res => {
        console.log(res);
        this.resetForm();
        this.refreshList();
      })
    } else {
      this.eService.update(this.eService.categoryData).subscribe(res => {
        this.resetForm();
        this.refreshList();
      })
    }
  }

  close() {
    this.resetForm();
        this.refreshList();
  }
 
  /*public getCategories = () => {
    this.eService.getData('Product_Category')
    .subscribe(res => {
      this.dataSource.data = res as Category[];
    })
  }
 
  public redirectToDetails = (id: string) => {
    
  }
 
  public redirectToUpdate = (id: string) => {
    
  }
 
  public redirectToDelete = (id: string) => {
    
  }
  
  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }*/

