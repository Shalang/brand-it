
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';

import { ErrorHandleService } from 'src/app/shared/errorhandle.service';
import { SuccessComponent } from 'src/app/shared/success/success.component';
import { CategoryService } from '../category.service';
import { Category } from '../category.model';

@Component({
  selector: 'app-updatecategory',
  templateUrl: './updatecategory.component.html',
  styleUrls: ['./updatecategory.component.css']
})
export class UpdatecategoryComponent implements OnInit {

  public categoryForm: FormGroup;
  private dialogConfig;
  public category: Category;
  

  constructor(private location: Location, private eService: CategoryService, private dialog: MatDialog, private errorService: ErrorHandleService,
              private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.categoryForm = new FormGroup({
      Description: new FormControl('', [Validators.required, Validators.maxLength(20)]),
    });

    this.dialogConfig = {
      height: '200px',
      width: '400px',
      disableClose: true,
      data: {}
    }

    this.getCategoryById();
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.categoryForm.controls[controlName].hasError(errorName);
  }

  public onCancel = () => {
    this.location.back();
  }

  private getCategoryById = () => {
    let Product_Category_ID: string = this.activeRoute.snapshot.params['id'];
      
    let categoryByIdUrl: string = `Product_Category/${Product_Category_ID}`;
   
    this.eService.getData(categoryByIdUrl)
      .subscribe(res => {
        this.category = res as Category;
        this.categoryForm.patchValue(this.category);
      },
      (error) => {
        this.errorService.dialogConfig = this.dialogConfig;
        this.errorService.handleError(error);
      })
  }

  public updateCategory = (formValue) => {
    if (this.categoryForm.valid) {
      this.executeUpdate(formValue);
    }
  }

  private executeUpdate = (formValue) => {

 
    this.category.Description = formValue.Description;
   
    let apiUrl = `Product_Category/${this.category.Product_Category_ID}`;
    this.eService.update(apiUrl, this.category)
      .subscribe(res => {
        let dialogRef = this.dialog.open(SuccessComponent, this.dialogConfig);
        
        dialogRef.afterClosed()
          .subscribe(result => {
            this.location.back();
          });
      },
      (error => {
        this.errorService.dialogConfig = this.dialogConfig;
        this.errorService.handleError(error);
      })
    )
  }

}
