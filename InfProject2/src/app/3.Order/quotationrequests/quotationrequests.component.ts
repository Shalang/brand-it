import { Component, OnInit, ViewChild } from '@angular/core';
//import { QuotationrequestService } from './quotationrequest.service';
//import { Quotationrequest } from './quotationrequest.model';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { HttpClientModule, HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-quotationrequests',
  templateUrl: './quotationrequests.component.html',
  styleUrls: ['./quotationrequests.component.css']
})

export class QuotationrequestsComponent implements OnInit {

  quotationrequests: string[];
  quotationrequestdetail: string[];
  constructor (private httpService: HttpClient, private router : Router/*, public qrservice: QuotationrequestService*/) { }
  //constructor(public qrservice: QuotationrequestService) { }
  
  ngOnInit() {
    this.httpService.get('http://localhost:44369/api/QuotationRequests').subscribe (data => {
      this.quotationrequests = data as string [];
      }
    );
     //this.getAllRequests();
  }
  requestdetails(id) {
    console.log('id')
    localStorage["Request_ID"] = id;
    this.httpService.get('http://localhost:44369/api/QuotationRequests/'+id).subscribe (res => {
      this.quotationrequestdetail = res as string [];
    });
  }

  generate(id) {
    localStorage["Request_ID"] = id;
    this.router.navigate(["/quotations"]);
  }
}
