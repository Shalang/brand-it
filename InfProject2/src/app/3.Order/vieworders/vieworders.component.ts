import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-vieworders',
  templateUrl: './vieworders.component.html',
  styleUrls: ['./vieworders.component.css']
})
export class ViewordersComponent implements OnInit {

  orders: string[];
  orderdetail: string[];
  constructor (private httpService: HttpClient) { }
  
  ngOnInit() {
    this.httpService.get('https://localhost:44369/api/Customer_Order/getCustomerOrders').subscribe (data => {
      this.orders = data as string [];
    });
}
cusorderdetails(id) {
  console.log('id')
   localStorage["Customer_Order_ID"] = id;
    this.httpService.get('https://localhost:44369/api/Customer_Order/getOrderDetails/'+id).subscribe (res => {
      this.orderdetail = res as string [];
    });
}


}
