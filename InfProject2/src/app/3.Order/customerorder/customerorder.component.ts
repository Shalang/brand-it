import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { CategoryService } from 'src/app/4.Product/category/category.service';
import { ErrorHandleService } from 'src/app/shared/errorhandle.service';
import { Customerorder } from './customerorder.model';

@Component({
  selector: 'app-customerorder',
  templateUrl: './customerorder.component.html',
  styleUrls: ['./customerorder.component.css']
})
export class CustomerorderComponent implements OnInit {

  
  public displayedColumns = ['Customer','Order Date', 'Order Total', 'Order Status', 'View'];
  public dataSource = new MatTableDataSource<Customerorder>();
  @ViewChild(MatSort) sort: MatSort;
 

  constructor(public eService: CategoryService, private errorService: ErrorHandleService, private router: Router) { }

  ngOnInit() {
    this.getAllOrders();
  }
 
  public getAllOrders = () => {
    this.eService.getData('Customer_Order')
    .subscribe(res => {
      this.dataSource.data = res as Customerorder[];
    },
    (error) => {
      this.errorService.handleError(error);
    })
    
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  public customSort = (event) => {
    console.log(event);
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }
 
  public details = (id: string) => {
    let url: string = `details/${id}`;
    this.router.navigate([url]);
  }

 
}
