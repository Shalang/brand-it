import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { CategoryService } from 'src/app/4.Product/category/category.service';
import { ErrorHandleService } from 'src/app/shared/errorhandle.service';
import { SuccessComponent } from 'src/app/shared/success/success.component';
import { Return } from '../return.model';

import { Location } from '@angular/common';

@Component({
  selector: 'app-createreturn',
  templateUrl: './createreturn.component.html',
  styleUrls: ['./createreturn.component.css']
})
export class CreatereturnComponent implements OnInit {

  public returnForm: FormGroup;
  private dialogConfig;

  constructor(private location: Location, private eService: CategoryService, private dialog: MatDialog, private errorService: ErrorHandleService) { }

  ngOnInit() {
    this.returnForm = new FormGroup({
     Price: new FormControl('', [Validators.required,  Validators.maxLength(20)]),
     Reason: new FormControl('', [Validators.required,  Validators.maxLength(20)]),
     Product: new FormControl('', [Validators.required,  Validators.maxLength(20)]),
    });

    this.dialogConfig = {
      height: '200px',
      width: '400px',
      disableClose: true,
      data: { }
    }
  }
 
  public hasError = (controlName: string, errorName: string) =>{
    return this.returnForm.controls[controlName].hasError(errorName);
  }
 
  public onCancel = () => {
    this.location.back();
  }
 
  public createReturn = (formValue) => {
    if (this.returnForm.valid) {
      this.executeCreation(formValue);
    }
  }
 
  private executeCreation = (formValue) => {
    let cReturn: Return = {
      Return_Customer_Order_ID : formValue.Return_Customer_Order_ID,
      Price : formValue.Price,
        Date : new Date(),
          Reason : formValue.Reason,
          Product : formValue.Product,
    }
  
 
    let apiUrl = 'Return_Customer_Order';
    this.eService.create(apiUrl, cReturn)
      .subscribe(res => {
        let dialogRef = this.dialog.open(SuccessComponent, this.dialogConfig);
 
        //we are subscribing on the [mat-dialog-close] attribute as soon as we click on the dialog button
        dialogRef.afterClosed()
          .subscribe(result => {
            this.location.back();
          });
      },
    )
  }
 
}