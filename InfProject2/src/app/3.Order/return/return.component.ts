import { Component, OnInit, ViewChild } from '@angular/core';
import { CategoryService } from 'src/app/4.Product/category/category.service';
import { ErrorHandleService } from 'src/app/shared/errorhandle.service';
import { Return } from './return.model';
import { SuccessComponent } from 'src/app/shared/success/success.component';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';

@Component({
  selector: 'app-return',
  templateUrl: './return.component.html',
  styleUrls: ['./return.component.css']
})
export class ReturnComponent implements OnInit {

 
  public displayedColumns = ['Price', 'Date', 'Reason', 'Product','details'];
  public dataSource = new MatTableDataSource<Return>();
  @ViewChild(MatSort) sort: MatSort;
 

  constructor(public eService: CategoryService, private errorService: ErrorHandleService, private router: Router) { }

  ngOnInit() {
    this.getAllReturns();
  }
 
  public getAllReturns = () => {
    this.eService.getData('Return_Customer_Order')
    .subscribe(res => {
      this.dataSource.data = res as Return[];
    },
    (error) => {
      this.errorService.handleError(error);
    })
    
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  public customSort = (event) => {
    console.log(event);
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }
 
  public details = (id: string) => {
    let url: string = `details/${id}`;
    this.router.navigate([url]);
  }
 
  public update = (id: string) => {
    let updateUrl: string = `update/${id}`;
    this.router.navigate([updateUrl]);
  }

  public delete = (id: string) => {
    let url: string = `delete/${id}`;
    this.router.navigate([url]);
  }
 
}