import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ReportService } from '../report.service';
import { Reporting } from '../reporting';
import{map} from 'rxjs/operators';
import {Chart} from 'Chart.js'
//declare let jsPDF;
//  import  'jspdf';
//  import 'jspdf-autotable';
// import jsPDF from 'jspdf';

// import autoTable from 'jspdf-autotable';
import {  Input, Inject } from '@angular/core';
@Component({
  selector: 'app-sales-report',
  templateUrl: './sales-report.component.html',
  styleUrls: ['./sales-report.component.scss']
})
export class SalesReportComponent implements OnInit {

  dateForm: any;  
  reportingClass:Reporting;
  map:any;
  data: boolean=false;
 
  constructor(private formbuilder: FormBuilder,private report:ReportService) { }

  ngOnInit(): void {
    this.dateForm = this.formbuilder.group({ 
      startDate: ['', [Validators.required]],  
      endDate: ['', [Validators.required]], 
    }); 
    
  }

  PAYMENTs: any ;
  SUMs:any;
  chart=[];
  title = 'angularReport';

minDate: Date = new Date ();
maxDate: Date = new Date ();
//Error Display
error:any={isError:false,errorMessage:''};
isValidDate:any;

  model : any={};    
  emp:any;  

onSubmit(){
// if(this.chart) this.chart.destroy();
debugger;
 this.chart=[];
  const date = this.dateForm.value;

  

  this.report.getDateData1(date).subscribe((res: any) => {  
    
    console.log(res); 
    

    this.PAYMENTs=res["Payment"] ; 
    this.SUMs=res["Sum"] ; 
    debugger;
    
    

    let values = res["Payment"].map(z=> z.Quantity);
 let keys = res["Payment"].map(z=> z.Description);
 


  var chart  = new Chart('canvas', {
   type: 'bar',
   data: {
       labels: keys,
       datasets: [{
           label: 'Quantities',
           data: values,
           barPercentage: 0.30,
           backgroundColor: 
               'rgba(0, 181, 204, 1)',
           
           
           borderColor: 
               'rgba(36, 37, 42, 1)',
           
           borderWidth: 1
       }]
   },
   options: {
     legend: {
       labels :{
         fontColor: 'Black',
         fontSize : 20
       }
     },
       scales: {
           yAxes: [{
             
               ticks: {
                 fontColor: 'Black',
                   beginAtZero: true
               }
           }],
           xAxes: [{
             
             ticks: {
               fontColor: 'Black',
               fontSize : 15
                 
             }
         }]
       }
   }
});

 })

}

//   DownloadPDF()
//   {
    
//     const date = this.dateForm.value;
//     this.report.getDateData1(date).subscribe((res: any) => {
 
//       let doc =new jsPDF();debugger;
//   var pageHeight=doc.internal.pageSize.height||doc.internal.pageSize.getHeight();
//    var pageWidth=doc.internal.pageSize.width||doc.internal.pageSize.getWidth();
  
//    let length=res["Payment"].length;
//   // let keys=res["Payment"].map(d=>d.Date);debugger;
   
//    let values=res["Sum"];
  
//   let finalY=160;
//   var newCanvas=<HTMLCanvasElement>document.querySelector("#canvas");
  
//   var newCanvasImg=newCanvas.toDataURL("images/png",1.0);
//   doc.setTextColor(255,0,0);
//   doc.setFontSize(20)
 
//   doc.setFont("courier");
//   doc.setFontSize(10)
//   doc.text("Address: 786 Jan Shoba St",(pageWidth/4.5)-30,30)
//   doc.setFont("courier");
//   doc.setFontSize(10)
//   doc.setFont("courier");
//   doc.text("Surbub:  Brooklyn",(pageWidth/4.5)-30,40)
//   doc.setFontSize(10)
//   doc.setFont("courier");
//   doc.text("City: Pretoria",(pageWidth/4.5)-30,50)
//   doc.setFontSize(10)
//   doc.setFont("courier");
//   doc.text("Postal Code: 0028",(pageWidth/4.5)-30,60)
//   doc.setFont("helvetica");
//   doc.setFont("times");
//   doc.setFontSize(20)
//   doc.text("Sales Report",(pageWidth/3)-15,75)
//   doc.addImage(newCanvasImg,"PNG",25,25,160,150);
//   doc.setFontSize(14)
//   // var img = new Image()
//   // img.src = './assets/Kimo.png'
//   // doc.addImage(img, 'png', 140, 15, 70, 50)
//   //doc.text(keys ,(pageWidth/2)-25,finalY+23)
//   doc.autoTable({startY:finalY-80,html:'#test',useCss:true,head:[['customerOrderID','Date','Description','Quantity','Price']], pageBreak: 'avoid',})
//    finalY=doc.autoTable.previous.finalY
  
//    doc.text("Total Sales: R"+values,(pageWidth/5.2)-25,finalY+10)
  
  
//   doc.save('Sales Report.pdf')
//   })
  
// }
}
  