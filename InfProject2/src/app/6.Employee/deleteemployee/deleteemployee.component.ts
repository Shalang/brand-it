import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//import { CategoryService } from '../4.Product/category/category.service';
import { MatDialog } from '@angular/material/dialog';
//import { ErrorHandleService } from '../shared/errorhandle.service';
//import { Category } from '../4.Product/category/category.model';
//import { SuccessComponent } from '../shared/success/success.component';
import { Location } from '@angular/common';
import { EmployeeService } from '../employee/employee.service';
import { ErrorHandleService } from 'src/app/shared/errorhandle.service';
import { Employee } from '../employee/employee.model';
import { SuccessComponent } from 'src/app/shared/success/success.component';
import { CategoryService } from 'src/app/4.Product/category/category.service';

@Component({
  selector: 'app-deleteemployee',
  templateUrl: './deleteemployee.component.html',
  styleUrls: ['./deleteemployee.component.css']
})

export class DeleteemployeeComponent implements OnInit {

  constructor(private location: Location, private eService: CategoryService, private dialog: MatDialog, private errorService: ErrorHandleService,
    private activeRoute: ActivatedRoute) { }

  private dialogConfig;
  public employee: Employee;

ngOnInit() {
  this.dialogConfig = {
    height: '200px',
    width: '400px',
    disableClose: true,
    data: {}
  }

  this.getEmployeeById();
}

public onCancel = () => {
  this.location.back();
}

private getEmployeeById = () => {
  let Employee_ID: string = this.activeRoute.snapshot.params['id'];
    
  let employeeByIdUrl: string = `Employees/${Employee_ID}`;
 
  this.eService.getData(employeeByIdUrl)
    .subscribe(res => {
      this.employee = res as Employee;
    },
    (error) => {
      this.errorService.dialogConfig = this.dialogConfig;
      this.errorService.handleError(error);
    })
}

public delete = () => {
  let deleteUrl: string = `Employees/${this.employee.Employee_ID}`;
  this.eService.delete(deleteUrl)
    .subscribe(res => {
      let dialogRef = this.dialog.open(SuccessComponent, this.dialogConfig);
      
      dialogRef.afterClosed()
        .subscribe(result => {
          this.location.back();
        });
    },
    (error) => {
      this.errorService.dialogConfig = this.dialogConfig;
      this.errorService.handleError(error);
    })
}

}
