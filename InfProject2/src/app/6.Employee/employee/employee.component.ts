import { Component, OnInit, ViewChild } from '@angular/core';
import { EmployeeService } from './employee.service';
import { Employee } from './employee.model';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { ErrorHandleService } from 'src/app/shared/errorhandle.service';
import { CategoryService } from 'src/app/4.Product/category/category.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})

export class EmployeeComponent implements OnInit {

  public displayedColumns = ['Name', 'Surname','Cell_Number', 'Email_Address', 'details', 'update', 'delete'];
  public dataSource = new MatTableDataSource<Employee>();
  //public dataSourceb = new MatTableDataSource<Employeetype>();
  @ViewChild(MatSort) sort: MatSort;
 

  constructor(public eService: EmployeeService, private errorService: ErrorHandleService, private router: Router) { }

  ngOnInit() {
    this.getAllEmployees();
    //this.getAllEmpTypes();;
  }
 
  public getAllEmployees = () => {
    this.eService.getData('Employees')
    .subscribe(res => {
      this.dataSource.data = res as Employee[];
    },
    (error) => {
      this.errorService.handleError(error);
    })
    
  }

// public getAllEmpTypes = () => {
//   this.eService.getData('Employee_Type')
//   .subscribe(res => {
//     this.dataSourceb.data = res as Employeetype[];
//   },
//   (error) => {
//     this.errorService.handleError(error);
//   })
  
// }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  public customSort = (event) => {
    console.log(event);
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }
 
  public details = (id: string) => {
    let url: string = `details/${id}`;
    this.router.navigate([url]);
  }
 
  public updateEmployee = (id: string) => {
    let updateUrl: string = `updateemployee/${id}`;
    this.router.navigate([updateUrl]);
  }

  public deleteEmployee = (id: string) => {
    let url: string = `deleteemployee/${id}`;
    this.router.navigate([url]);
  }

 
}


  /*ngOnInit() {
    this.resetForm();
    this.refreshList();
  }
  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  resetForm() {
    this.eService.categoryData = {
      Product_Category_ID: 0,
      Description: ''      
    }
    this.eService.categoryList = [];  
  }

  

  public refreshList = () => {
    this.eService.getData('Product_Category')
    .subscribe(res => {
      this.dataSource.data = res as Category[];
    })
  }

  editCategory(cat: Category) {
    this.eService.categoryData = cat;   
  }

  deleteCategory(id: number) {
    this.eService.delete(id).subscribe(res => {
      this.resetForm();
      this.refreshList();
    })
  }

  submit() {
    if(this.eService.categoryData.Product_Category_ID === 0) {
      this.eService.create(this.eService.categoryData).subscribe(res => {
        console.log(res);
        this.resetForm();
        this.refreshList();
      })
    } else {
      this.eService.update(this.eService.categoryData).subscribe(res => {
        this.resetForm();
        this.refreshList();
      })
    }
  }

  close() {
    this.resetForm();
        this.refreshList();
  }
 
  /*public getCategories = () => {
    this.eService.getData('Product_Category')
    .subscribe(res => {
      this.dataSource.data = res as Category[];
    })
  }
 
  public redirectToDetails = (id: string) => {
    
  }
 
  public redirectToUpdate = (id: string) => {
    
  }
 
  public redirectToDelete = (id: string) => {
    
  }
  
  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }*/

