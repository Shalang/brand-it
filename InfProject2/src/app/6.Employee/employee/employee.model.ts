export class Employee {
    Employee_ID: number;
    Name: string;
    Surname: string;
    Cell_Number: string;
    Email: string;
    Address: number;
    Employee_Type_ID: number;
    User_ID: number
}
