import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Employee } from '../employee/employee.model';
import { Location } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { SuccessComponent } from 'src/app/shared/success/success.component';
import { ErrorHandleService } from 'src/app/shared/errorhandle.service';
import { EmployeeService } from '../employee/employee.service';
import { Department } from '../department.model';

@Component({
  selector: 'app-createemployee',
  templateUrl: './createemployee.component.html',
  styleUrls: ['./createemployee.component.css']
})

export class CreateemployeeComponent implements OnInit {
  
 


  public employeeForm: FormGroup;
  private dialogConfig;

  constructor(private location: Location, public eService: EmployeeService, private dialog: MatDialog, private errorService: ErrorHandleService) { }

  ngOnInit() {
    this.employeeForm = new FormGroup({
      Name: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      Surname: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      Cell_Number: new FormControl('', [Validators.required, Validators.maxLength(10)]),
      Email: new FormControl('', [Validators.required, Validators.maxLength(50), Validators.email]),
      Employee_Type: new FormControl,

      
    });
    
    

    this.dialogConfig = {
      height: '200px',
      width: '400px',
      disableClose: true,
      data: { }
    }
  }
 


  public hasError = (controlName: string, errorName: string) =>{
    return this.employeeForm.controls[controlName].hasError(errorName);
  }
 
  public onCancel = () => {
    this.location.back();
  }
 
  public createEmployee = (formValue) => {
    if (this.employeeForm.valid) {
      this.executeCreation(formValue);
    }
  }
 
  private executeCreation = (formValue) => {
    let employee: Employee = {
      Employee_ID: formValue.Employee_ID,
      Name: formValue.Name,
      Surname: formValue.Surname,
      Cell_Number: formValue.Cell_Number,
      Email: formValue.Email,
      Address: formValue.Address,
      Employee_Type_ID: formValue.Employee_Type,
      User_ID: formValue.User_ID
     
    }
 
    let apiUrl = 'Employees';
    this.eService.create(apiUrl, employee)
      .subscribe(res => {
        let dialogRef = this.dialog.open(SuccessComponent, this.dialogConfig);
 
        //we are subscribing on the [mat-dialog-close] attribute as soon as we click on the dialog button
        dialogRef.afterClosed()
          .subscribe(result => {
            this.location.back();
          });
      },
    )
  }
 
}