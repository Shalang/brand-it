import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Reporting } from './reporting';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor( public http: HttpClient) { }
  getDateData(date: Reporting): Observable<Reporting> {  
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
    return this.http.post<Reporting>('http://localhost:44369/Api/CustomerReturnReport/getData/',  
    date, httpOptions);  
  }  

getReportData(date:Reporting):Observable<Reporting>{    
  return this.http.get<Reporting>('http://localhost:44369/Api/CustomerReturnReport/getData/'+date).pipe(map(result=>result))
  ;    
}  
getDateData1(date: Reporting): Observable<Reporting> {  
  const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
  return this.http.post<Reporting>('http://localhost:44369/Api/SalesReport/getData/',  
  date, httpOptions);  
}  

getReportData1(date:Reporting):Observable<Reporting>{    
return this.http.get<Reporting>('http://localhost:44369/Api/SalesReport/getData/'+date).pipe(map(result=>result))
;    
}  
}
