import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms'

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CategoryComponent } from './4.Product/category/category.component';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';



import { MatFormFieldModule } from '@angular/material/form-field';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatTableModule } from '@angular/material/table';
import { SuccessComponent } from './shared/success/success.component';
import { ErrorComponent } from './shared/error/error.component'
import { MatTabsModule } from '@angular/material/tabs';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MatCommonModule } from '@angular/material/core';
import { MatDialogModule} from '@angular/material/dialog';

import { ErrorhandleComponent } from './shared/errorhandle/errorhandle.component';



import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SupplierComponent } from './5.Supplier/supplier/supplier.component';

import { ProducttypeComponent } from './4.Product/producttype/producttype.component';

import { ReceiveorderComponent } from './5.Supplier/receiveorder/receiveorder.component';
import { SendrequestComponent } from './5.Supplier/sendrequest/sendrequest.component';
import { TemplateComponent } from './4.Product/template/template.component';
import { CreatetemplateComponent } from './4.Product/createtemplate/createtemplate.component';
import { JobtypeComponent } from './4.Product/jobtype/jobtype.component';
import { CompletetempComponent } from './2.Customer/completetemp/completetemp.component';
import { CustomernavbarComponent } from './customernavbar/customernavbar.component';
import { CustomerComponent } from './2.Customer/customer/customer.component';
import { DeletecustomerComponent } from './2.Customer/deletecustomer/deletecustomer.component';
import { CreateemployeeComponent } from './6.Employee/createemployee/createemployee.component';
import { DeleteemployeeComponent } from './6.Employee/deleteemployee/deleteemployee.component';
import { EmployeeComponent } from './6.Employee/employee/employee.component';
import { SizeComponent } from './4.Product/size/size.component';
import { DeletecategoryComponent } from './4.Product/category/deletecategory/deletecategory.component';
import { CreatecategoryComponent } from './4.Product/category/createcategory/createcategory.component';
import { UpdatecategoryComponent } from './4.Product/category/updatecategory/updatecategory.component';
import { DeletejobtypeComponent } from './4.Product/jobtype/deletejobtype/deletejobtype.component';
import { CreatejobtypeComponent } from './4.Product/jobtype/createjobtype/createjobtype.component';
import { UpdatejobtypeComponent } from './4.Product/jobtype/updatejobtype/updatejobtype.component';
import { CreatesizeComponent } from './4.Product/size/createsize/createsize.component';
import { DeletesizeComponent } from './4.Product/size/deletesize/deletesize.component';
import { UpdatesizeComponent } from './4.Product/size/updatesize/updatesize.component';
import { DeletetemplateComponent } from './4.Product/template/deletetemplate/deletetemplate.component';
import { UpdatetemplateComponent } from './4.Product/template/updatetemplate/updatetemplate.component';
import { QuotationsComponent } from './quotations/quotations.component';
import { QuotationrequestsComponent } from './3.Order/quotationrequests/quotationrequests.component';
import { QuotationrequestComponent } from './quotationrequest/quotationrequest.component';
import { ReturncusorderComponent } from './returncusorder/returncusorder.component';
import { ReturnComponent } from './3.Order/return/return.component';
import { CreatereturnComponent } from './3.Order/return/createreturn/createreturn.component';
import { CreatesupplierComponent } from './5.Supplier/createsupplier/createsupplier.component';
import { DeletesupplierComponent } from './5.Supplier/deletesupplier/deletesupplier.component';
import { UpdatesupplierComponent } from './5.Supplier/updatesupplier/updatesupplier.component';
import { CreateproducttypeComponent } from './4.Product/createproducttype/createproducttype.component';
import { CustomerorderComponent } from './3.Order/customerorder/customerorder.component';
import { SupplierordersComponent } from './supplierorders/supplierorders.component';
import { SupplierorderlineComponent } from './supplierorders/supplierorderline/supplierorderline.component';
import { SupplierorderComponent } from './supplierorders/supplierorder/supplierorder.component';
import { SalesReportComponent } from './sales-report/sales-report.component';
import { CustomerReturnComponent } from './customer-return/customer-return.component';
import { ViewordersComponent } from './3.Order/vieworders/vieworders.component';
import { HomeComponent } from './home/home.component';

//import { OrderdetailsComponent } from './orderdetails/orderdetails.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    CategoryComponent,
    SupplierComponent,
    DeletecategoryComponent,
    ProducttypeComponent,
    UpdatecategoryComponent,
    ReceiveorderComponent,
    SendrequestComponent,
    CreatecategoryComponent,

    SuccessComponent,
    ErrorComponent,
    ErrorhandleComponent,
    TemplateComponent,
    CreatetemplateComponent,
    DeletetemplateComponent,
    UpdatetemplateComponent,
    JobtypeComponent,
    CreatejobtypeComponent,
    DeletejobtypeComponent,
    UpdatejobtypeComponent,
    CompletetempComponent,
    CustomernavbarComponent,
    CustomerComponent,
    DeletecustomerComponent,
    
    CreateemployeeComponent,
    DeleteemployeeComponent,
    EmployeeComponent,
    JobtypeComponent,
    SizeComponent,
    DeletesizeComponent,
    UpdatesizeComponent,
    CreatesizeComponent,
    QuotationsComponent,
    QuotationrequestsComponent,
    QuotationrequestComponent,
    ReturncusorderComponent,
    ReturnComponent,
    CreatereturnComponent,
    SupplierComponent,
    CreatesupplierComponent,
    DeletesupplierComponent,
    UpdatesupplierComponent,
    CreateproducttypeComponent,
    CustomerorderComponent,
    SupplierordersComponent,
    SupplierorderlineComponent,
    //OrderdetailsComponent,
    SupplierorderComponent,
    SalesReportComponent,
    CustomerReturnComponent,
    ViewordersComponent,
    HomeComponent
    

    
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatCommonModule,
    
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatCheckboxModule,
    MatChipsModule,
    MatTableModule,
    FlexLayoutModule,
    MatTabsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    MatSortModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatProgressBarModule,  
    MatProgressSpinnerModule,
    MatCardModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
  //  RouterModule.forRoot(routes, { useHash: true })
  ],
  providers: [  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
