import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompletetempComponent } from './2.Customer/completetemp/completetemp.component';
import { CustomerComponent } from './2.Customer/customer/customer.component';
import { DeletecustomerComponent } from './2.Customer/deletecustomer/deletecustomer.component';
import { CustomerorderComponent } from './3.Order/customerorder/customerorder.component';
import { QuotationrequestsComponent } from './3.Order/quotationrequests/quotationrequests.component';
import { CreatereturnComponent } from './3.Order/return/createreturn/createreturn.component';
import { ReturnComponent } from './3.Order/return/return.component';
import { ViewordersComponent } from './3.Order/vieworders/vieworders.component';
import { CategoryComponent } from './4.Product/category/category.component';
import { CreatecategoryComponent } from './4.Product/category/createcategory/createcategory.component';
import { DeletecategoryComponent } from './4.Product/category/deletecategory/deletecategory.component';
import { UpdatecategoryComponent } from './4.Product/category/updatecategory/updatecategory.component';
import { CreateproducttypeComponent } from './4.Product/createproducttype/createproducttype.component';
import { CreatejobtypeComponent } from './4.Product/jobtype/createjobtype/createjobtype.component';
import { DeletejobtypeComponent } from './4.Product/jobtype/deletejobtype/deletejobtype.component';
import { JobtypeComponent } from './4.Product/jobtype/jobtype.component';
import { UpdatejobtypeComponent } from './4.Product/jobtype/updatejobtype/updatejobtype.component';
import { ProducttypeComponent } from './4.Product/producttype/producttype.component';
import { CreatesizeComponent } from './4.Product/size/createsize/createsize.component';
import { DeletesizeComponent } from './4.Product/size/deletesize/deletesize.component';
import { SizeComponent } from './4.Product/size/size.component';
import { UpdatesizeComponent } from './4.Product/size/updatesize/updatesize.component';
import { CreatesupplierComponent } from './5.Supplier/createsupplier/createsupplier.component';
import { DeletesupplierComponent } from './5.Supplier/deletesupplier/deletesupplier.component';
import { SupplierComponent } from './5.Supplier/supplier/supplier.component';
import { UpdatesupplierComponent } from './5.Supplier/updatesupplier/updatesupplier.component';
import { CreateemployeeComponent } from './6.Employee/createemployee/createemployee.component';
import { DeleteemployeeComponent } from './6.Employee/deleteemployee/deleteemployee.component';
import { EmployeeComponent } from './6.Employee/employee/employee.component';
import { CustomerReturnComponent } from './customer-return/customer-return.component';
import { HomeComponent } from './home/home.component';
import { QuotationrequestComponent } from './quotationrequest/quotationrequest.component';
import { QuotationsComponent } from './quotations/quotations.component';
import { ReturncusorderComponent } from './returncusorder/returncusorder.component';
import { SalesReportComponent } from './sales-report/sales-report.component';
import { SupplierorderComponent } from './supplierorders/supplierorder/supplierorder.component';
import { SupplierorderlineComponent } from './supplierorders/supplierorderline/supplierorderline.component';
import { SupplierordersComponent } from './supplierorders/supplierorders.component';




const routes: Routes = [

  {path: 'category', component: CategoryComponent},
  {path: 'createcategory', component: CreatecategoryComponent},  
  {path: 'delete/:id',  component: DeletecategoryComponent},  
  { path: 'update/:id', component: UpdatecategoryComponent },

  {path: 'customer', component: CustomerComponent},   
  {path: 'deletecustomer/:id',  component: DeletecustomerComponent},  

  {path: 'employee', component: EmployeeComponent}, 
  {path: 'createemployee',  component: CreateemployeeComponent},
  {path: 'deleteemployee/:id',  component: DeleteemployeeComponent},
   

  {path: 'jobtype', component: JobtypeComponent},   
  {path: 'createjobtype', component: CreatejobtypeComponent},  
  {path: 'deletejobtype/:id',  component: DeletejobtypeComponent},  
  { path: 'updatejobtype/:id', component: UpdatejobtypeComponent },
  {path: 'size', component: SizeComponent},   
  {path: 'createsize', component: CreatesizeComponent},  
  {path: 'deletesize/:id',  component: DeletesizeComponent},  
  { path: 'updatesize/:id', component: UpdatesizeComponent },
 
  { path: 'completetemp', component: CompletetempComponent },
  { path: 'quotations', component: QuotationsComponent },
  {path: 'quotationrequests', component: QuotationrequestsComponent},
  {path: 'quotationrequest', component: QuotationrequestComponent},
  {path:'createproducttype', component: CreateproducttypeComponent},

  {path:'producttype', component: ProducttypeComponent},

  { path: 'returncusorder', component: ReturncusorderComponent },
  { path: 'return', component: ReturnComponent },
  { path: 'createreturn', component: CreatereturnComponent },
  { path: 'supplier', component: SupplierComponent },
  { path: 'createsupplier', component: CreatesupplierComponent },
  { path: 'deletesupplier/:id', component: DeletesupplierComponent },
  { path: 'updatesupplier/:id', component: UpdatesupplierComponent },
  { path: 'customerorder', component: CustomerorderComponent },
  { path: 'supplierorder', component: SupplierorderComponent },
  { path: 'supplierorderline', component: SupplierorderlineComponent },
  { path: 'supplierorders', component: SupplierordersComponent },
  { path: 'CustomerReturnReport', component: CustomerReturnComponent },
  { path: 'SalesReport', component: SalesReportComponent },
  { path: 'vieworders', component: ViewordersComponent },
  { path: 'home', component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
