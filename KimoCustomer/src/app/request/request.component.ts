import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { Prodtypes } from '../products/prodtype.model';
import { Designs } from '../products/prodtype.model';
import { DesignImage } from '../products/prodtype.model';
import { DesignLine } from '../products/prodtype.model';

import { DesignService } from '../shared/design.service';
import { Router } from '@angular/router';
import { CATCH_STACK_VAR } from '@angular/compiler/src/output/abstract_emitter';
import { CATCH_ERROR_VAR } from '@angular/compiler/src/output/output_ast';
import { Weblinks } from '../weblinks.model';
import { Prodsize } from '../prodsize.model';
import { Quote } from '@angular/compiler';
import { Quotereqline } from '../shared/quotereqline.model';
import { QuotationrequestService } from '../shared/quotationrequest.service';
import { Quotationrequest } from '../shared/quotationrequest.model';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {


  types: Prodtypes[];
  designs: DesignImage[];
  designline: Designs[];
  lines : DesignLine[];
  designType : any;
  request = [];

  CurrentRequest: Quotereqline;
  RequestList: Quotereqline[] = [];

  TodayDate : string;

  webLinks : Weblinks[];
  sizes : Prodsize[];

  constructor(public service : DesignService, private qrservice:QuotationrequestService, private fb: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.TodayDate = new Date().toDateString();
    this.CurrentRequest = new Quotereqline();
    this.service.getTypes().then(res => this.types = res as Prodtypes[]);

    this.service.getSizes().subscribe( (sizes) => {
      this.sizes = sizes;
    })
    this.service.getWebLinks().subscribe( (weblinks) => {

      this.webLinks = weblinks;

    })
  }
  
  store(id, name){
    this.CurrentRequest.Product_Type_ID = id;
    this.service.getProductTypeImage(name).then(resp => this.designs = resp as DesignImage[]);

    this.designType = this.service.getDesign(localStorage["Product_Type_ID"]).subscribe( (designType) => {
      this.designType = designType
    } );

    console.log(this.designType)

    this.service.getProductTypes(id,localStorage["Customer_ID"]).then( (respo) => 
    {
      this.designline = respo as Designs[]
    });

    localStorage["Product_Type_ID"] = id;
  }

  storeDesign(id){
    this.service.getDesignLines(id).then(response => this.lines = response as DesignLine[]);
    localStorage["Design_ID"] = id;
    this.CurrentRequest.Design_ID = id;
  }

  addToRequest(color,website,productCode,quantity){
    this.RequestList.push(this.CurrentRequest);
    this.CurrentRequest = new Quotereqline();
    console.log(this.RequestList);

  }

  delete(i){
    //this.request.splice(i,1);
    this.RequestList.splice(i,1);
  }

  addRequest(){
    //console.log(this.request.length)
    var NewQuote = new Quotationrequest();
    NewQuote.Customer_ID = parseInt(localStorage.getItem("Customer_ID"));

    this.qrservice.CreateQuotationRequest(NewQuote).then( (request) => {

      console.log(request);
      this.RequestList.forEach(element => {
        
        element.Request_ID = request.Request_ID;

      });
      return this.RequestList;

    }).then( () =>
    {

      this.qrservice.AddQuoteLines(this.RequestList).subscribe( (res) => {

        console.log('All the requests now have the same RequestID')
        this.RequestList = [];
        this.designs = [];
        this.designline = [];
        this.CurrentRequest = new Quotereqline();

      })
    }

    )
}

  navigate()
  {
    this.router.navigate(["design"]);
  }

}