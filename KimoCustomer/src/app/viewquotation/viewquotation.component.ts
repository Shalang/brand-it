import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { QuotationrequestService } from '../shared/quotationrequest.service';
import { QuoteVM } from '../shared/quote-vm';
import { QuoteLineVM } from '../shared/quote-line-vm';

@Component({
  selector: 'app-viewquotation',
  templateUrl: './viewquotation.component.html',
  styleUrls: ['./viewquotation.component.css']
})
export class ViewquotationComponent implements OnInit {

  quotations: string[];
  quotationdetail: string[];

  QuoteList : QuoteVM[] = [];
  CurrentQuoteLines : QuoteLineVM[];
  CurrentQuoteID : number;

  constructor (private httpService: HttpClient , public quotationRequestService : QuotationrequestService) { }
  
  ngOnInit() {
    this.httpService.get('https://localhost:44369/api/Quotation/getQuotations').subscribe (data => {
      this.quotations = data as string [];
    });

      this.quotationRequestService
      .GetCustomerQuotes( parseInt(localStorage['Customer_ID']) )
      .subscribe( (quoteVMList) => {

        this.QuoteList = quoteVMList;
        console.log(this.QuoteList);

      })

}

  QuoteDetails(id){

    var CurrentQuote = this.QuoteList.find( ql => ql.Quote_ID == id );
    this.CurrentQuoteID = id;

    this.CurrentQuoteLines = CurrentQuote.quoteLineVMs;

  }

  AcceptQuotation( ){

    this.quotationRequestService.CreateCustomerOrder().then( (res) => {

      this.quotationRequestService.AcceptQuotation( this.CurrentQuoteID , res.Customer_Order_ID )
      .subscribe( (res) => {

        console.log('Customer Order has been created!')

      })

    })

  }

}
