﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;
using System.Web.Http.Description;
using Kimo.Models;

namespace Kimo.Controllers
{
    public class Customer_OrderController : ApiController
    {
        private KIMODBEntities db = new KIMODBEntities();

        //Sending email to the employee for delivery
        [Route("api/Customer_Order/SendMail")]
        [HttpPost]
        [AllowAnonymous]
        public string SendEmail()
        {
            SmtpClient emp = new SmtpClient();
            emp.DeliveryMethod = SmtpDeliveryMethod.Network;
            emp.EnableSsl = true;
            emp.Host = "smtp.gmil.com";
            emp.Port = 587;

            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("u17345180@tuks.co.za", "Pringles98");
            emp.UseDefaultCredentials = false;
            emp.Credentials = credentials;

            MailMessage msg = new MailMessage();
            msg.From = new MailAddress("u17345180@tuks.co.za");
            msg.To.Add(new MailAddress("lebonchabeleng12@gmail.com"));

            msg.Subject = "Package delivery";
            msg.IsBodyHtml = true;
            msg.Body = "The order is ready for delivery";

            try
            {
                emp.Send(msg);
                return "OK";
            }
            catch (Exception ex)
            {

            }
            return "OK";
        }

        [System.Web.Http.Route("api/Customer_Order/AcceptQuotation/{QuoteID}/{CustomerOrderID}")]
        [HttpGet]
        public IHttpActionResult GetCustomerQuotations(int QuoteID , int CustomerOrderID)
        {
            var CurrentQuote = db.Quotations
                .Include( q => q.Quotation_Line )
                .Where(q => q.Quotation_ID == QuoteID)
                .FirstOrDefault();

            foreach (var quoteLine in CurrentQuote.Quotation_Line)
            {
                Customer_Order_Line customer_Order_Line = new Customer_Order_Line();
                customer_Order_Line.Customer_Order_ID = CustomerOrderID;
                customer_Order_Line.Design_ID = quoteLine.Design_ID;
                customer_Order_Line.Price = quoteLine.Price;
                customer_Order_Line.Product_Size_ID = quoteLine.Product_Size_ID;
                customer_Order_Line.Product_Type_ID = quoteLine.Product_Type_ID;
                customer_Order_Line.Product_Code = quoteLine.Product_Code;
                customer_Order_Line.Quantity = quoteLine.Quantity;
                customer_Order_Line.Web_Link_ID = quoteLine.Web_Link_ID;
                customer_Order_Line.Description = quoteLine.Description;

                db.Customer_Order_Line.Add(customer_Order_Line);

            }

            db.SaveChanges();

            var DeleteQuoute = db.Quotations.Where(q => q.Quotation_ID == QuoteID).FirstOrDefault();
            var DeleteQuoteLines = db.Quotation_Line.Where(ql => ql.Quotation_ID == QuoteID).ToList();

            db.Quotation_Line.RemoveRange(DeleteQuoteLines);
            db.SaveChanges();

            db.Quotations.Remove(DeleteQuoute);
            db.SaveChanges();

            return Ok();

        }

        //View order
        [System.Web.Http.Route("api/Customer_Order/getCustomerOrders")]
        [HttpGet]
        public System.Object GetCustomerOrders()
        {
            var result = (from a in db.Customer_Order
                          join b in db.Customer_Order_Line on a.Customer_Order_ID equals b.Customer_Order_ID
                          join c in db.Order_Status on a.OrderStatus_ID equals c.OrderStatus_ID
                          select new
                          {
                              a.Customer_Order_ID,
                              Customer_ID = a.Customer.First_Name,
                              b.Description,
                              //a.Order_Status.OrderStatus_ID,
                              OrderStatus_ID = c.Description,
                              a.Order_Date,
                              a.Order_Total,

                          }).ToList();
            return result;
        }

        [System.Web.Http.Route("api/Customer_Order/GetOrderDetails/{id}")]
        [HttpGet]

        public System.Object getOrderDetails(int id)
        {
            var orderdetails = (from a in db.Customer_Order
                                join b in db.Customer_Order_Line on a.Customer_Order_ID equals b.Customer_Order_ID
                                where a.Customer_Order_ID == id
                                select new
                                {
                                    a.Customer_Order_ID,
                                    b.Description,
                                    b.Quantity,
                                    b.Price,
                                    a.Order_Date

                                }).ToList();
            return orderdetails;
        }
        //private KIMODBEntities db = new KIMODBEntities();

        //// GET: api/Customer_Order
        //public IQueryable<Customer_Order> GetCustomer_Order()
        //{
        //    return db.Customer_Order;
        //}

        // GET: api/Customer_Order/5
        [ResponseType(typeof(Customer_Order))]
        [System.Web.Http.Route(Name = "GetCustomer_Order")]
        public IHttpActionResult GetCustomer_Order(int id)
        {
            Customer_Order customer_Order = db.Customer_Order.Find(id);
            if (customer_Order == null)
            {
                return NotFound();
            }

            return Ok(customer_Order);
        }

        //// PUT: api/Customer_Order/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutCustomer_Order(int id, Customer_Order customer_Order)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != customer_Order.Customer_Order_ID)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(customer_Order).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!Customer_OrderExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        // POST: api/Customer_Order
        [ResponseType(typeof(Customer_Order))]
        public IHttpActionResult PostCustomer_Order(Customer_Order customer_Order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            customer_Order.Order_Date = DateTime.Now.Date;
            customer_Order.OrderStatus_ID = 1;

            db.Customer_Order.Add(customer_Order);
            db.SaveChanges();

            return CreatedAtRoute("GetCustomer_Order", new { id = customer_Order.Customer_Order_ID }, customer_Order);
        }

        //// DELETE: api/Customer_Order/5
        //[ResponseType(typeof(Customer_Order))]
        //public IHttpActionResult DeleteCustomer_Order(int id)
        //{
        //    Customer_Order customer_Order = db.Customer_Order.Find(id);
        //    if (customer_Order == null)
        //    {
        //        return NotFound();
        //    }

        //    db.Customer_Order.Remove(customer_Order);
        //    db.SaveChanges();

        //    return Ok(customer_Order);
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        //private bool Customer_OrderExists(int id)
        //{
        //    return db.Customer_Order.Count(e => e.Customer_Order_ID == id) > 0;
        //}
    }
}